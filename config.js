const DB = require("./src/database");
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const url = `mongodb://${DB.URL}:${DB.PORT}/${DB.NAME}`;

mongoose.connect(url, { ...DB.OPTIONS });
mongoose.connection.once('open', () => console.log(`Connected to mongo at ${url}`));