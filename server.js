'use strict';

const PORT = 4000;
const HOST = '0.0.0.0';

const express               = require('express');
const { ApolloServer, gql } = require('apollo-server-express');
require('./config');
const { Allowance } = require('./models');

const typeDefs = gql`
scalar Date,
    type Allowance {
        id: ID!
        description: String,
        allowance: Boolean,
        amount: Int,
        date: Date,
        createdAt: Date
    }
    type Query {
        getAllowance: [Allowance]
    }
    type Mutation {
        addTransaction(description: String!, date: Date, allowance: Boolean!,amount: Int! ): Allowance
    }
    
`;

/*
app.post("/transaction", (req, res) => {
  console.log(`Saving new transaction: ${JSON.stringify(req.body)}`);
  const { description, allowance, amount, date } = req.body;
  let newTransaction = new Allowance({
    description,
    allowance: allowance || false,
    amount: allowance ? amount : -amount,
    date: date || Date.now()
  });
  // total: (last !== null ? last.total : 10) - req.body.amount
  newTransaction.save().then(res => {
    console.log(`Transaction ${res._id} saved`);
  });

  return res.send(req.body);
});

*/

const resolvers = {
    Query: {
        getAllowance: async () => await Allowance.find({}).exec()
    },
    Mutation: {
        addTransaction: async (_, args) => {
            try {
                let response = await Allowance.create(args);
                return response;
            } catch(e) {
                return e.message;
            }
        }
    }
};

const server = new ApolloServer({ typeDefs, resolvers });
const app = express();
server.applyMiddleware({ app });

app.listen({ port: PORT,host:HOST }, () =>
  console.log(`🚀 Server ready at http://${HOST}:${PORT}${server.graphqlPath}`)
);