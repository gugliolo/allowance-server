const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
    createdAt: { type: Date, default: Date.now },
    date: { type: Date, default: Date.now },
    description: String,
    allowance: Boolean,
    amount: Number
  });
  
    const Allowance = mongoose.model("Allowance", transactionSchema);

  module.exports = { Allowance}